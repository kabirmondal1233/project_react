import './App.css';

let name = "Kabir Mondal";

let currentDate = new Date().toLocaleDateString();
let currentTime = new Date().toLocaleTimeString();
let image = "https://picsum.photos/200/300";
let image1 = "https://picsum.photos/230/300";
let image2 = "https://picsum.photos/280/300";
let image3 = "https://picsum.photos/300/300";
 
let heads = {
 color :"red",
 textAlign : "center"
}

function App() {
  return (
    <>
    <h1 style = {heads}>Welcome to Localhost : 3000</h1>
    <p style = {heads}>Today Date is {currentDate}</p>
    <p style = {heads}>Current Time is a {currentTime}</p>
    <div className='header'>
    <h1>Hello, My Name Is {name}</h1>
    <img src={image} alt='randomImage'/>
    <img src={image1} alt='randomImage'/>
    <img src={image2} alt='randomImage'/>
    <img src={image3} alt='randomImage'/>
    </div>
    </>
  );
}

export default App;
